package com.potugi.price_history_service;

import org.springframework.data.repository.CrudRepository;

public interface CurrencyPairsInfoRepository extends
        CrudRepository<CurrencyPairsInfo, Long> {
}
