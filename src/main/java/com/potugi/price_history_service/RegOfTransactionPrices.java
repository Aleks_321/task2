package com.potugi.price_history_service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Component
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegOfTransactionPrices {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPk;

    private Long idFk;
    private double value;
    private String timeStamp;

}
