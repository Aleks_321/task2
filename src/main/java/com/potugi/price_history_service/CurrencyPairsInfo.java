package com.potugi.price_history_service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class CurrencyPairsInfo {
    @Id
    @GeneratedValue
    private long id;

    private String currencyPairName;
}
