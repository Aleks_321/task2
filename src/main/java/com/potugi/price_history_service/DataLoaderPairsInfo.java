package com.potugi.price_history_service;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
class DataLoaderPairsInfo {
    private final CurrencyPairsInfoRepository repo;
    public DataLoaderPairsInfo(CurrencyPairsInfoRepository repo) {
        this.repo = repo;
    }

    @PostConstruct
    private void loadData() {
        repo.saveAll(List.of(
                new CurrencyPairsInfo(1, "RUBUSD"),
                new CurrencyPairsInfo(2, "EURUSD"),
                new CurrencyPairsInfo(3, "USDJPY")
        ));
    }
}