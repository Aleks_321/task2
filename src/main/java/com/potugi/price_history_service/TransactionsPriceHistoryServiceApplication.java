package com.potugi.price_history_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionsPriceHistoryServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransactionsPriceHistoryServiceApplication.class, args);
    }
}
