package com.potugi.price_history_service;

import org.springframework.web.bind.annotation.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/historytransactions")
public class GetHistoryTransaction {
    private final RegOfTransactionPricesRepository repoTransaction;
    private final CurrencyPairsInfoRepository repoPairsInfo;

    public GetHistoryTransaction(RegOfTransactionPricesRepository repoTransaction
    , CurrencyPairsInfoRepository repoPairsInfo) {
        this.repoTransaction = repoTransaction;
        this.repoPairsInfo = repoPairsInfo;
    }

    @GetMapping
    private Iterable<RegOfTransactionPrices>
    pollHistoryTransactions(@RequestParam(value = "pair_Name", required = false) String pairName
            ,@RequestParam(value = "date_From", required = false) String dateFrom
            ,@RequestParam(value = "date_To", required = false) String dateTo) {

        Instant from = Instant.parse(dateFrom);
        Instant to = Instant.parse(dateTo);


        List<RegOfTransactionPrices> ans = new ArrayList<>();
        long idFk = 0;

        for(CurrencyPairsInfo itForId : repoPairsInfo.findAll()) {
            if (itForId.getCurrencyPairName().equals(pairName)) {
                idFk = itForId.getId();
                break;
            }
        }

        for (RegOfTransactionPrices it:  repoTransaction.findAll()) {
            if (idFk == it.getIdFk()
                    && Instant.parse(it.getTimeStamp()).isAfter(from)
                    && Instant.parse(it.getTimeStamp()).isBefore(to)) {
                ans.add(it);
            }
        }
        return ans;
    }
}
