package com.potugi.price_history_service;

import org.springframework.data.repository.CrudRepository;

public interface RegOfTransactionPricesRepository
        extends CrudRepository<RegOfTransactionPrices, Long> {
}
