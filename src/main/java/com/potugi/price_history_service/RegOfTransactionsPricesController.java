package com.potugi.price_history_service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
@EnableScheduling
@RequiredArgsConstructor
public class RegOfTransactionsPricesController {
    @NonNull
    final CurrencyPairsInfoRepository repoPairsInfo;
    @NonNull
    final RegOfTransactionPricesRepository repoTransoctions;
    private WebClient client;

    @Scheduled(fixedRate = 10000)
    private void pollTransactions() {
        for (CurrencyPairsInfo currentPairsInfo : repoPairsInfo.findAll()) {
            String CurrPairName = currentPairsInfo.getCurrencyPairName();

            client = WebClient.create("http://localhost:7634/curencypairs/" + CurrPairName);

            client.get()
                    .retrieve()
                    .bodyToFlux(RegOfTransactionPrices.class)
                    .toStream()
                    .forEach(currentTransactionInfo -> {
                        repoTransoctions.save(new RegOfTransactionPrices(currentTransactionInfo.getIdPk()
                                , currentPairsInfo.getId()
                                , currentTransactionInfo.getValue()
                                , currentTransactionInfo.getTimeStamp().toString()));
                    });
        }
    }
}
